# The Shape Library of OpenSCAD
There are a lot of shapes that are not available by default in [OpenSCAD](https://www.openscad.org/). So this is a library to get the necessary shapes that are not available in OpenSCAD by default.

## Installation
To use a library in OpenSCAD, you simply need to download [shapes.scad](./shapes.scad) into the same folder as your file. 

Then, at the top of your file, simply write:

```scad
include <shapes.scad>
```

## Documentation and usage
This is the usage of every shapes of this library.

You can also find the basic shapes installed by defuault in the [OpenSCAD cheat sheet](https://www.openscad.org/cheatsheet/)

### pyramid(h, w, l);
This function will render a basic square-base pyramid. This is an example:

```scad
pyramid(100, 50, 50);
```

### prism(l, w, h);
This function will render a *polyhedron* of a triangular prism.

![prism](https://upload.wikimedia.org/wikipedia/commons/e/ea/Triangular_prism_wedge.png)

Example:

```scad
prism(10,10,10);
```

### curve(w, h, l, dh);
> [source](https://stackoverflow.com/questions/54115749/how-to-a-make-a-curved-sheet-cube-in-openscad)

This function will render a *curved sheet*

The variables works like this:
![schema](https://i.stack.imgur.com/fV1eJ.png)

This is an example:

![example](https://i.stack.imgur.com/F0eVM.png)

```scad
curve(10, 2, 25, 2);
```

## Copyright
Note that some snippets presented here are not made by me and are found from the internet. Same for the images. The copyright of the images are in the quotes.

Otherwise this project is under GNU-GPL-v3.