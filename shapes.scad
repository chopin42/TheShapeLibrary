// Basic library
module pyramid(h, w, l) { // Height, Width, Length
    polyhedron(
        points=[ [w,l,0],[w,-l,0],[-w,-l,0],[-w,l,0], // the four points at base
               [0,0,h] ],                                 // the apex point 
        faces=[ [0,1,4],[1,2,4],[2,3,4],[3,0,4],              // each triangle side
              [1,0,3],[2,1,3] ]                         // two triangles for square base
 );
}

module prism(l, w, h){
       polyhedron(
               points=[[0,0,0], [l,0,0], [l,w,0], [0,w,0], [0,w,h], [l,w,h]],
               faces=[[0,1,2,3],[5,4,3,2],[0,4,5,1],[0,3,4],[5,2,1]]
       );
}

module curve(width, height, length, dh) {
    r = ((length/2)*(length/2) - dh*dh)/(2*dh);
    a = asin((length/2)/r);
    rotate_extrude(angle = a) translate([r, 0, 0]) square(size = [height, width], center = true);
}

// Need more work
module trapeze() {
    difference() {
        rotate([0,0,45])
            cylinder(d1=sqrt(pow(17,2)*2), d2=0, h=14.17, $fn=4);
   
        translate([0,0,2])
            linear_extrude(14)
                square([17,17], center=true);
    }
} 